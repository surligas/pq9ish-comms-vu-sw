/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pq9ish.h"
#include "aprs.h"
#include "stm32l4xx_hal.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

extern RTC_HandleTypeDef hrtc;

static uint8_t __aprs_report[256];

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon)
{
  int ret;

  if(!conf || !lon || !lat) {
    return -PQ9ISH_INVALID_PARAM;
  }

  /* Sanity checks for lon/lat coordinates */
  if(strnlen(lon, APRS_LON_STR_LEN) != APRS_LON_STR_LEN
      || strnlen(lat, APRS_LAT_STR_LEN) != APRS_LAT_STR_LEN) {
    return -PQ9ISH_INVALID_PARAM;
  }

  memcpy(conf->lon, lon, APRS_LON_STR_LEN);
  memcpy(conf->lat, lat, APRS_LAT_STR_LEN);

  ret = ax25_init(&conf->hax25, dest_addr, dest_ssid, src_addr, src_ssid,
                  AX25_PREAMBLE_LEN, AX25_POSTAMBLE_LEN,
                  AX25_UI_FRAME, 0x3, 0xF0);
  if(ret) {
    return ret;
  }

  return PQ9ISH_SUCCESS;
}


/**
 * Get an APRS compatible (DHM) weather timestamp in UTC
 * @param out the string to hold the timestamp. The string should be
 * at least 8 bytes according to the APRS specification (7 digits + null termination).
 * The resulting string is a valid null terminated string.
 * @return
 */
static int
aprs_get_timestamp(char *out)
{
  int ret;
  RTC_DateTypeDef date;
  RTC_TimeTypeDef time;
  if(!out) {
    return -PQ9ISH_INVALID_PARAM;
  }
  ret = HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  ret = HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  sprintf(out, "%02u", date.Date);
  sprintf(out + 2, "%02u", time.Hours);
  sprintf(out + 4, "%02u", time.Minutes);
  out[6] = 'z';
  out[7] = 0;
  return PQ9ISH_SUCCESS;
}


int
aprs_tx_packet(aprs_conf_t *conf, ax5043_conf_t *hax5043, const tx_data_t *tx_data)
{
  int ret;
  int plen, binlen;
  char bin_str[9];

  if(tx_data->sequence % 10 == 0) {
    plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		     ">PQ9ISH OBC and COMMS status line");
    if(plen < 1) {
      return -PQ9ISH_INVALID_PARAM;
    }
      
    enable_pa(hax5043);
    ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);
  }
  itoa(tx_data->dval, bin_str, 2);
  binlen = strlen((bin_str));
  memmove(bin_str+8-binlen, bin_str, binlen+1);
  memset(bin_str, '0', 8-binlen);
  plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		   "T#%03d,%03d,%03d,%03d,%03d,%03d,%8s", tx_data->sequence % 1000, tx_data->aval1,
		   tx_data->aval2, tx_data->aval3, tx_data->aval4, tx_data->aval5, bin_str);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }

  enable_pa(hax5043);
  ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);


  plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		   ":%-9s:PARM.av1,av2,av3,av4,av5,b1,b2,b3,b4,b5,b6,b7,b8", CALLSIGN_DESTINATION);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }

  enable_pa(hax5043);
  ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);


  plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		   ":%-9s:UNIT.n,n,n,n,n,on,on,on,on,on,on,on,on", CALLSIGN_DESTINATION);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }

  enable_pa(hax5043);
  ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);


  plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		   ":%-9s:EQNS.0,1,0,0,1,0,0,1,0,0,1,0,0,1,0", CALLSIGN_DESTINATION);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }

  enable_pa(hax5043);
  ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);

  plen = snprintf ((char *) __aprs_report, AX25_MAX_FRAME_LEN,
		   ":%-9s:BITS.11111111,PQ9ISH OBC and COMMS", CALLSIGN_DESTINATION);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }

  enable_pa(hax5043);
  ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);

  enable_pa(hax5043);
  return ret |= ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, plen);
}

int
aprs_fmt_tlm_msg(const tx_data_t *tx_data, char *outbuf)
{
  int plen, binlen;
  char bin_str[9];

  itoa(tx_data->dval, bin_str, 2);
  binlen = strlen((bin_str));
  memmove(bin_str+8-binlen, bin_str, binlen+1);
  memset(bin_str, '0', 8-binlen);
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   "T#%03d,%03d,%03d,%03d,%03d,%03d,%8s",
		   tx_data->sequence % 1000,
		   tx_data->aval1,
		   tx_data->aval2,
		   tx_data->aval3,
		   tx_data->aval4,
		   tx_data->aval5,
		   bin_str);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}

int
aprs_fmt_tlm_param_msg(const char *vname1,
		       const char *vname2,
		       const char *vname3,
		       const char *vname4,
		       const char *vname5,
		       const char *bname1,
		       const char *bname2,
		       const char *bname3,
		       const char *bname4,
		       const char *bname5,
		       const char *bname6,
		       const char *bname7,
		       const char *bname8,
		       char *outbuf)
{
  int plen;
  
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   ":%-9s:PARM.%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		   CALLSIGN_DESTINATION,
		   vname1,
		   vname2,
		   vname3,
		   vname4,
		   vname5,
		   bname1,
		   bname2,
		   bname3,
		   bname4,
		   bname5,
		   bname6,
		   bname7,
		   bname8);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}

int
aprs_fmt_tlm_units_msg(const char *unit1,
		       const char *unit2,
		       const char *unit3,
		       const char *unit4,
		       const char *unit5,
		       const char *lbl1,
		       const char *lbl2,
		       const char *lbl3,
		       const char *lbl4,
		       const char *lbl5,
		       const char *lbl6,
		       const char *lbl7,
		       const char *lbl8,
		       char *outbuf)
{
  int plen;
  
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   ":%-9s:UNIT.%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		   CALLSIGN_DESTINATION,
		   unit1,
		   unit2,
		   unit3,
		   unit4,
		   unit5,
		   lbl1,
		   lbl2,
		   lbl3,
		   lbl4,
		   lbl5,
		   lbl6,
		   lbl7,
		   lbl8);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}

int
aprs_fmt_tlm_eqns_msg(const char *eq1a, const char *eq1b, const char *eq1c,
		      const char *eq2a, const char *eq2b, const char *eq2c,
		      const char *eq3a, const char *eq3b, const char *eq3c,
		      const char *eq4a, const char *eq4b, const char *eq4c,
		      const char *eq5a, const char *eq5b, const char *eq5c,
		      char *outbuf)
{
  int plen;
  
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   ":%-9s:EQNS.%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		   CALLSIGN_DESTINATION,
		   eq1a, eq1b, eq1c,
		   eq2a, eq2b, eq2c,
		   eq3a, eq3b, eq3c,
		   eq4a, eq4b, eq4c,
		   eq5a, eq5b, eq5c);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}

int
aprs_fmt_tlm_bitspn_msg(const char *bs1,
			const char *bs2,
			const char *bs3,
			const char *bs4,
			const char *bs5,
			const char *bs6,
			const char *bs7,
			const char *bs8,
			const char *proj_name,
			char *outbuf)
{
  int plen;
  
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   ":%-9s:BITS.%s%s%s%s%s%s%s%s,%s",
		   CALLSIGN_DESTINATION,
		   bs1,
		   bs2,
		   bs3,
		   bs4,
		   bs5,
		   bs6,
		   bs7,
		   bs8,
		   proj_name);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}

int
aprs_fmt_status_msg(const char *message, char * outbuf)
{
  int plen;
  
  plen = snprintf (outbuf, AX25_MAX_FRAME_LEN,
		   ">%s", message);
  if(plen < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  return plen;
}
