/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "stm32l4xx_hal.h"
#include <FreeRTOSConfig.h>
#include <FreeRTOS.h>
#include <cmsis_os.h>

extern uint32_t __millis;

#define BIT(x)   (1 << (x))
#define TICKS_PER_US (configCPU_CLOCK_HZ / 1000000U)

static inline size_t
min_ul(size_t x, size_t y)
{
  size_t ret = x < y ? x : y;
  return ret;
}

/**
 * Delays the execution of the task for the specified amount of microseconds
 * @param us the microseconds to sleep
 */
static inline void
usleep (uint32_t us)
{
  uint32_t us_ticks = TICKS_PER_US * us;
  us_ticks = us_ticks == 0 ? 1 : us_ticks;
  osDelay(10);
}

/**
 *
 * @return the milliseconds from the start of the program.
 * Time is handled based on TIM2 timer.
 */
static inline uint32_t
millis()
{
  return __millis;
}

static inline uint32_t
usecs()
{
  return HAL_GetTick() / TICKS_PER_US;
}

#endif /* UTILS_H_ */
