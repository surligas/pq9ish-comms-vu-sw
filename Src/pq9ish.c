/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017,2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pq9ish.h>
#include <stdio.h>
#include <string.h>
#include "conf.h"
#include "ax5043.h"
#include "aprs.h"
#include "spi_utils.h"
#include "utils.h"

#define TEST_LAT "4903.50N"
#define TEST_LON "07201.75W"

void
enable_pa(ax5043_conf_t *hax)
{
  if(!hax) {
    return;
  }
  ax5043_set_antsel(hax, 0, 0, AX5043_RF_SWITCH_ENABLE);
  ax5043_enable_pwramp(hax, AX5043_EXT_PA_ENABLE);
}

static void
disable_pa(ax5043_conf_t *hax)
{
  if(!hax) {
    return;
  }
  ax5043_enable_pwramp(hax, AX5043_EXT_PA_DISABLE);
  ax5043_set_antsel(hax, 0, 0, AX5043_RF_SWITCH_DISABLE);
}

int
pq9ish_rf_init(pq9ish_rf_conf_t *conf)
{
  int ret;
  if(!conf) {
    return -PQ9ISH_INVALID_PARAM;
  }

  conf->hax5043.read = ax5043_spi_read;
  conf->hax5043.write = ax5043_spi_write;
  conf->hax5043.delay_us = usleep;
  conf->hax5043.spi_select = ax5043_device_select;

  ret = ax5043_init(&conf->hax5043, XTAL_FREQ_HZ, VCO_INTERNAL,
                    &disable_pa);
  if(ret) {
    return ret;
  }
  ret = aprs_init(&conf->haprs, CALLSIGN_DESTINATION, 0, CALLSIGN_STATION, 0, APRS_WEATHER, TEST_LAT,
                  TEST_LON);
  if(ret) {
    return ret;
  }
  conf->framing = TX_AX25;
  return PQ9ISH_SUCCESS;
}

int
pq9ish_tx(pq9ish_rf_conf_t *conf, const tx_data_t *data)
{
  int ret;

  enable_pa(&conf->hax5043);
  if (conf->framing == TX_AX25) {
    ret = ax25_tx_frame(&conf->haprs.hax25, &conf->hax5043, (uint8_t *) data, sizeof(tx_data_t));
  }
  else {
    ret = ax5043_tx_frame(&conf->hax5043, (uint8_t*)data, sizeof(data), &conf->haprs.hax25.preamble_len, &conf->haprs.hax25.postable_len,
			  1000);
  }
  return ret;
}

